package matriz_adyacencia;

import java.util.Arrays;

public class MatrizAdyacencia {
	private int[] vector;
	private int cantidad_de_nodos;
	private static final int INFINITO = Integer.MAX_VALUE;

	public MatrizAdyacencia(int n) {
		cantidad_de_nodos = n;
		vector = new int[(int) ((Math.pow(n, 2)-n)/2)];
		Arrays.fill(vector, INFINITO);
	}
	
	private int calcular(int F, int C) {
		if (F > C) 
			return calcular(C,F);
		
		return (int) (F*cantidad_de_nodos+C-(Math.pow(F, 2)+3*F+2)/2);
	}
	
	public int getFC(int F, int C) {
		int i = calcular(F, C);
		return vector[i];
	}
	
	public void setFC(int F, int C, int costo) {
		int i = calcular(F, C);
		vector[i] = costo;
	}
	
	public int getCantidadDeNodos() {
		return cantidad_de_nodos;
	}
	
	public static int getInfinito() {
		return INFINITO;
	}
}
