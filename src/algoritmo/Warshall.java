package algoritmo;

public class Warshall {

	boolean[][] f_impar;
	boolean[][] f_par;
	int cant_nodos;
	
	public Warshall(int[][] matriz, int n) {
		f_impar = new boolean[n][n];
		f_par = new boolean[n][n];
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				f_par[i][j] = matriz[i][j] < 50000 && matriz[i][j] != 0;
			}
		}
		cant_nodos = n;
	}
	
	public void ejecutar() {
		
		for (int k = 0; k < cant_nodos; k++) {
			
			for (int i = 0; i < cant_nodos; i++) {
				
				for (int j = 0; j < cant_nodos; j++) 
					if (i==k||j==k||i==j) 
						f_impar[i][j]  = f_par[i][j];
					else
						f_impar[i][j]  = f_par[i][j] | f_par[i][k] & f_par[k][j];
			}
			if (k+1 < cant_nodos) {
				k++;
				for (int i = 0; i < cant_nodos; i++) {
					
					for (int j = 0; j < cant_nodos; j++) 
						if (i==k||j==k||i==j) 
							f_par[i][j]  = f_impar[i][j];
						else
							f_par[i][j]  = f_impar[i][j] | f_impar[i][k] & f_impar[k][j];
				}
			}
		}
	}
	
	public static void main(String[] args) {
		int[][] mat = new int[3][3];
		
		for (int i = 0; i < mat.length; i++) {
			mat[i][i] = 0;
			for (int j = i+1; j < mat.length; j++) {
				mat[i][j] = 50000;
				mat[j][i] = 50000;
			}
		}
		
		
		mat[0][2]=8;
		mat[1][0]=5;
		mat[2][1]=3;
		
		Warshall test = new Warshall(mat, 3);
		test.ejecutar();
		
		for (boolean[] e : test.f_impar) {
			System.out.println(e[0] +" "+ e[1] +" "+e[2] +" ");
		}
	}

}
