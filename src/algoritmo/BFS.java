package algoritmo;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import matriz_adyacencia.MatrizAdyacencia;

public class BFS {
	
	int nodos = 7;
	public int D[] = new int[nodos];
	Queue<Integer> cola = new LinkedList<>();
	Boolean[] visitado = new Boolean[nodos];
	MatrizAdyacencia grafo = new MatrizAdyacencia(nodos);
	
	private void ejecutar() {
		Boolean flag = false;
		cola.add(0);
		Arrays.fill(D, MatrizAdyacencia.getInfinito());
		Arrays.fill(visitado, false);
		D[0]=0;
		visitado[0] = true;
		int x=1; 
		while (!cola.isEmpty()) {
			int nodoact = cola.remove();
			visitado[nodoact] = true;
			
			
			for (int j = 0; j < nodos; j++) {
				if (j!=nodoact) 
					if (grafo.getFC(nodoact, j) != MatrizAdyacencia.getInfinito() && visitado[j] == false) {
						cola.add(j);
						x++;
						D[j] = D[nodoact]+1;
						flag = true;
					}
			}
			if (flag) {
				flag = false;
			}else {
				System.out.println("ciclo");
				//x--;
			}
				
			
		}
		System.out.println("Cantidad de nodos visitados "+x);
	}
	
	public static void main(String[] args) {
		BFS test = new BFS();
		test.grafo.setFC(0, 1, 1);
		test.grafo.setFC(1, 2, 1);
		test.grafo.setFC(2, 3, 1);
		test.grafo.setFC(0, 4, 1);
		test.grafo.setFC(4, 5, 1);
		//test.grafo.setFC(4, 6, 1);
		
		test.ejecutar();
		
		for (int e : test.D) {
			System.out.print(e+" ");
		}
	}
}
