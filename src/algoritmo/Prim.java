package algoritmo;

import java.util.ArrayList;
import matriz_adyacencia.MatrizAdyacencia;

public class Prim {
	private int[] padre;
	private ArrayList<Integer> nodoVisitado;
	private ArrayList<Integer> nodoSinVisitar;
	private MatrizAdyacencia mAdy;
	private MatrizAdyacencia arbolAbarcador;
	private int costoMinimo = 0;
	
	public Prim(MatrizAdyacencia mAdy) {
		this.mAdy = mAdy;
		nodoVisitado = new ArrayList<>();
		nodoSinVisitar = new ArrayList<>();
		padre = new int[mAdy.getCantidadDeNodos()];
		for (int i = 0; i < padre.length; i++) { 
			padre[i]=i;
			nodoSinVisitar.add(i);
		}
		arbolAbarcador = new MatrizAdyacencia(mAdy.getCantidadDeNodos());
	}
	
	public void ejecutar() {
		nodoVisitado.add(nodoSinVisitar.remove(0));
		int costoMin = MatrizAdyacencia.getInfinito();
		int costoTemp = 0;
		int nodoCreceRama = 0;
		int nodoMasCercano = 0;
		while (!nodoSinVisitar.isEmpty()) {
			for (Integer nodoact : nodoVisitado) {

				for (Integer e : nodoSinVisitar) {
					// si existe arista
					if ((mAdy.getFC(nodoact, e)) < MatrizAdyacencia.getInfinito())
						//si no asigne constomin o costo de arista es menor qe costomin
						if (costoMin == MatrizAdyacencia.getInfinito() || mAdy.getFC(nodoact, e) < costoMin) {
							if (find(nodoact) != find(e)) {
								// el nodo es el conectable m�s cercano
								costoMin = mAdy.getFC(nodoact, e);
								nodoMasCercano = e;
								nodoCreceRama = nodoact;
							}
						}
					
				}
			}
			union(nodoCreceRama, nodoMasCercano);
			arbolAbarcador.setFC(nodoCreceRama, nodoMasCercano, costoMin);
			nodoVisitado.add(nodoMasCercano);
			nodoSinVisitar.remove(nodoSinVisitar.indexOf(nodoMasCercano));
			costoMinimo += costoMin;
			costoMin = MatrizAdyacencia.getInfinito();
			
		}
	}
	
	public int find(int x) {
		if (x == padre[x]) 
			return x;
		
		return find(padre[x]);
	}
	
	public void union(int x, int y) {
		int xPadre = find(x);
		int yPadre = find(y);
		
		padre[xPadre] = yPadre; 
	}
	
	public static void main(String[] args) {
		MatrizAdyacencia vec = new MatrizAdyacencia(3);
		vec.setFC(0, 1, 5);
		vec.setFC(0, 2, 2);
		vec.setFC(1, 2, 3);
		Prim test = new Prim(vec);
		test.ejecutar();
		System.out.println(test.costoMinimo);
		for (int i = 0; i < 3; i++) {
			for (int j = i+1; j < 3; j++) {
				System.out.print(test.arbolAbarcador.getFC(i, j)+" ");
			}
		}
	}
}
