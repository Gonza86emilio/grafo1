package algoritmo;

import java.util.ArrayList;
import java.util.Arrays;

import matriz_adyacencia.MatrizAdyacencia;

public class Dijkstra {
	int[] D;
	int[] vecCamino;
	int W;
	int nodoAct;
	ArrayList<Integer> S;
	ArrayList<Integer> V;
	int[][] mAdy;
	
	static int nodo = 5;
	public Dijkstra(int[][] vec, int nodoInicial) {
		D = new int[nodo];
		W = 0;
		vecCamino = new int[nodo];
		nodoAct = nodoInicial;
		S = new ArrayList<>();
		V = new ArrayList<>();
		mAdy = vec;
		
		for (int i = 0; i < nodo; i++) {
			V.add(i);
		}
	}
	
	public void ejecutar() {
		int min = 0;
		for (int i = 0; i < nodo; i++) {
			if (i != nodoAct) {
				D[i] = mAdy[nodoAct][i];
			}
		}
		
		S.add(nodoAct);
		V.remove(V.indexOf(nodoAct));
		
		for (int i = 0; i < nodo-1; i++) {
			W = minimo();
			S.add(V.remove(V.indexOf(W)));
			for (int j = 0; j < V.size(); j++) {
				if ((min = Math.min(D[V.get(j)], D[W]+mAdy[W][V.get(j)])) != D[V.get(j)]) {
					vecCamino[V.get(j)]=W;
					D[V.get(j)] = min;
				}
				//D[V.get(j)] = Math.min(D[V.get(j)], D[W]+mAdy[W][V.get(j)]);
			}
		}
		
	}
	
	
	/**
	 *  Busca y devuelve el indice del nodo con menor costo
	 * 
	 * @return el indice del nodo con menor costo
	 */
	private int minimo() {
		int minimo = V.get(0);
		for (int i : V) {
			if (D[i] < D[minimo]) {
				minimo = i;
			}
		}
		return minimo; 
	}

	public static void main(String[] args) {
		int[][] matriz = new int[nodo][nodo];
		Arrays.fill(matriz[0], 10000);
		Arrays.fill(matriz[1], 10000);
		Arrays.fill(matriz[2], 10000);
		Arrays.fill(matriz[3], 10000);
		Arrays.fill(matriz[4], 10000);
		matriz[0][1]=4;
		matriz[0][3]=1;
		matriz[0][4]=6;
		matriz[1][2]=1;
		matriz[2][4]=2;
		matriz[3][4]=7;
		matriz[3][2]=2;
		int nodoinicial = 0;
		Dijkstra test = new Dijkstra(matriz, nodoinicial);
		test.ejecutar();
		for (int e : test.D) {
			System.out.print(e+" ");
		}
		System.out.println();
		for (int e : test.vecCamino) {
			System.out.print(e+" ");
		}
	}

}
