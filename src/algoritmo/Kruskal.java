package algoritmo;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import matriz_adyacencia.MatrizAdyacencia;

public class Kruskal {
	private int[] padre;
	private int minimoCosto = 0;
	private MatrizAdyacencia mAdy;
	private List<Arista> aristas;
	private MatrizAdyacencia arbolAbarcador;
	
	public Kruskal(MatrizAdyacencia vec) {
		mAdy = vec;
		padre = new int[vec.getCantidadDeNodos()];
		aristas = new ArrayList<>();
		for (int i = 0; i < vec.getCantidadDeNodos(); i++) 
			for (int j = i+1; j < vec.getCantidadDeNodos(); j++) 
				if (mAdy.getFC(i, j) != MatrizAdyacencia.getInfinito()) 
					aristas.add(new Arista(i, j, mAdy.getFC(i, j)));
		arbolAbarcador = new MatrizAdyacencia(vec.getCantidadDeNodos());
		Collections.sort(this.aristas);
	}
	
	public void ejecutar() {
		int nodo1;
		int nodo2;
		int peso;
		
		// recorro la lista de aristas
		for (Arista arista : this.aristas) {
			// leo los datos de la arista
			nodo1 = arista.getNodo1();
			nodo2 = arista.getNodo2();
			peso = arista.getPeso();
			
			// si los nodos de la arista no tienen el mismo padre
			if (find(nodo1) != find(nodo2)) {
				// les pongo el mismo padre;
				union(nodo1, nodo2);
				// agrego la arista al arbol abarcador m�nimo
				this.arbolAbarcador.setFC(nodo1, nodo2, peso);
				// acumulo el peso de la arista al costo m�nimo
				this.minimoCosto += peso;
			}
		}
	}
	
	public int find(int x) {
		if (x == padre[x]) 
			return x;
		
		return find(padre[x]);
	}
	
	public void union(int x, int y) {
		int xPadre = find(x);
		int yPadre = find(y);
		
		padre[xPadre] = yPadre; 
	}
	
	class Arista implements Comparable<Arista>{
		int nodo1;
		int nodo2;
		int peso;
		public Arista(int nodo1, int nodo2, int peso) {
			this.nodo1=nodo1;
			this.nodo2=nodo2;
			this.peso=peso;
		}
		
		@Override
		public int compareTo(Arista o) {
			return o.peso - this.peso;
		}

		public int getNodo1() {
			return nodo1;
		}

		public void setNodo1(int nodo1) {
			this.nodo1 = nodo1;
		}

		public int getNodo2() {
			return nodo2;
		}

		public void setNodo2(int nodo2) {
			this.nodo2 = nodo2;
		}

		public int getPeso() {
			return peso;
		}

		public void setPeso(int peso) {
			this.peso = peso;
		}
		
		
	}
	
	
	public static void main(String[] args) {
		MatrizAdyacencia vec = new MatrizAdyacencia(3);
		vec.setFC(0, 1, 5);
		vec.setFC(0, 2, 2);
		vec.setFC(1, 2, 3);
		Kruskal test = new Kruskal(vec);
		test.ejecutar();
		System.out.println(test.minimoCosto);
		for (int i = 0; i < 3; i++) {
			for (int j = i+1; j < 3; j++) {
				System.out.print(test.arbolAbarcador.getFC(i, j)+" ");
			}
		}
	}
}
